#ifndef _pins_h
#define _pins_h

//
// Pins
//
#define CONFIG_TX_GPIO_NUM 33
#define CONFIG_RX_GPIO_NUM 25
#define RELAY_1_PIN 27
#define RELAY_2_PIN 14
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   25

#endif
