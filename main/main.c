#include <bits/stdint-uintn.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "driver/gpio.h"
#include "esp_bit_defs.h"
#include "freertos/FreeRTOS.h"
#include "freertos/portmacro.h"
#include "freertos/task.h"
#include "driver/spi_master.h"
#include "esp_log.h"

#include "hal/gpio_types.h"
#include "pins.h"
#include "spi_mod.h"

#define TAG "main"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"

#include "driver/twai.h"

#define CAN_TEMP_ID 0xc800501
#define CAN_RELAY_STATUS 0xd000502
// CAN relay command:
// BIT0 = 1 : relay 1 ON, BIT1 = 1: relay 1 OFF, BIT0 & BIT1 = 1: return Relay
// state;
// BIT2 = 1 : relay 1 ON, BIT3 = 1: relay 1 OFF, BIT2 & BIT3 = 1: return Relay
// state;
#define CAN_RELAY_CMD 0xc800503

// Configure GPIO pins for relay
void gpio_pin_config(void) {
  gpio_config_t io_conf = {
    .intr_type = GPIO_INTR_DISABLE,
    .mode = GPIO_MODE_OUTPUT,
    .pin_bit_mask = RELAY_1_PIN | RELAY_2_PIN,
    .pull_down_en = 0,
    .pull_up_en = 0
  };
  // configure GPIO with the given settings
  gpio_config(&io_conf);
  // active low pins, setting HIGH to off relay channels
  gpio_set_level(RELAY_1_PIN, 1);
  gpio_set_level(RELAY_2_PIN, 1);
}

void temp_task(void * pvParams) {
  uint16_t data;
  spi_transaction_t tM = {
    .tx_buffer = NULL,
    .rx_buffer = &data,
    .length = 16,
    .rxlength = 16,
  };

  while (true) {
    float temp1, temp2;

    // Temp 1
    spi_device_acquire_bus(spi1, portMAX_DELAY);
    spi_device_transmit(spi1, &tM);
    spi_device_release_bus(spi1);

    int16_t res = (int16_t) SPI_SWAP_DATA_RX(data, 16);

    if (res & BIT2) {
      ESP_LOGE(TAG, "Sensor is not connected\n");
      continue;
    }

    res >>= 3;
    temp1 = res * 0.25f;
    printf("SPI res=%d temp1=%f\n", res, temp1);

    // Temp 2
    spi_device_acquire_bus(spi2, portMAX_DELAY);
    spi_device_transmit(spi2, &tM);
    spi_device_release_bus(spi2);

    res = (int16_t)SPI_SWAP_DATA_RX(data, 16);

    if (res & BIT2) {
      ESP_LOGE(TAG, "Sensor is not connected\n");
      continue;
    }

    res >>= 3;
    temp2 = res * 0.25f;
    printf("SPI res=%d temp2=%f\n", res, temp2);

    // Transmit temperature via CAN
    // mount msg and send it
    twai_message_t msg;
    msg.identifier = CAN_TEMP_ID;
    msg.rtr = 0;
    msg.extd = 1;
    msg.data_length_code = 8;
    memcpy(msg.data, (uint8_t *)&temp1, 4);
    memcpy(&msg.data[4], (uint8_t *)&temp2, 4);
    twai_transmit(&msg, pdMS_TO_TICKS(200));
    // wait 1s
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
}

//
// TASK
// Receive TWAI messages
//
void twai_receive_task(void *pvParameters) {
  while (true) {
    twai_message_t msg;
    bool send_relay_status = false;

    msg.data_length_code = 0;
    if (twai_receive(&msg, portMAX_DELAY) != ESP_OK)
      continue;

    if (msg.identifier != CAN_RELAY_CMD)
      continue;

    if (msg.data[0] & BIT0 && msg.data[0] & BIT1)
      send_relay_status = true;
    else if (msg.data[1] & BIT0)
      gpio_set_level(RELAY_1_PIN, 0); // relay 1 ON: active LOW
    else if (msg.data[1] & BIT1)
      gpio_set_level(RELAY_1_PIN, 1); // relay 1 OFF: active LOW

    if (msg.data[0] & BIT2 && msg.data[0] & BIT3)
      send_relay_status = true;
    else if (msg.data[1] & BIT2)
      gpio_set_level(RELAY_2_PIN, 0); // relay 2 ON: active LOW
    else if (msg.data[1] & BIT3)
      gpio_set_level(RELAY_2_PIN, 1); // relay 2 OFF: active LOW

    if (send_relay_status) {
      uint8_t data = 0;
      msg.identifier = CAN_RELAY_STATUS;
      data |= gpio_get_level(RELAY_1_PIN) ? BIT0 : 0;
      data |= gpio_get_level(RELAY_2_PIN) ? BIT1 : 0;
      msg.extd = 1;
      msg.data_length_code = 1;
      msg.data[0] = data;
      twai_transmit(&msg, portMAX_DELAY);
    }
  }
  vTaskDelete(NULL);
}

// Configure TWAI (CAN) interface
void twai_config(void) {
  twai_general_config_t g_config = TWAI_GENERAL_CONFIG_DEFAULT(
      (gpio_num_t)CONFIG_TX_GPIO_NUM, (gpio_num_t)CONFIG_RX_GPIO_NUM,
      TWAI_MODE_NORMAL);
  twai_timing_config_t t_config = TWAI_TIMING_CONFIG_125KBITS();
  twai_filter_config_t f_config = TWAI_FILTER_CONFIG_ACCEPT_ALL();

  ESP_LOGI(TAG, "TX_PIN=%d RX_PIN=%d", CONFIG_TX_GPIO_NUM, CONFIG_RX_GPIO_NUM);

  // Install TWAI driver
  if (twai_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
    ESP_LOGI(TAG, "TWAI Driver installed\n");
  } else {
    printf("Failed to install TWAI driver\n");
    return;
  }

  // Start TWAI driver
  if (twai_start() == ESP_OK) {
    ESP_LOGI(TAG, "TWAI Driver started");
  } else {
    ESP_LOGI(TAG, "Failed to start TWAI driver");
    return;
  }
}

void app_main() {
  spi_init();
  twai_config();
  gpio_pin_config();
  xTaskCreate(&temp_task, "temperature_task", 4096, NULL, 5, NULL);
}
